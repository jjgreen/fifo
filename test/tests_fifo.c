/*
  tests_fifo.c
  Copyright (c) J.J. Green 2014
*/

#include <fifo.h>
#include "tests_fifo.h"

CU_TestInfo tests_fifo[] =
  {
    {"fill/empty", test_fifo_fill_and_empty},
    {"size argument", test_fifo_bad_size},
    {"max argument", test_fifo_max},
    {"cheap contract", test_fifo_cheap_contract},
    {"nontrivial expand", test_fifo_nontivial_expand},
    CU_TEST_INFO_NULL,
  };

/*
  run a typical duty cycle
*/

extern void test_fifo_fill_and_empty(void)
{
  fifo_t *f;

  f = fifo_new(sizeof(int), 5, 0);
  CU_ASSERT(f != NULL);

  for (int i = 0 ; i < 32 ; i++)
    CU_ASSERT(fifo_enqueue(f, &i) == 0);

  for (int i = 0 ; i < 32 ; i++)
    {
      int j;

      CU_ASSERT(fifo_dequeue(f, &j) == FIFO_OK);
      CU_ASSERT(i == j);
    }

  int k = 436;

  CU_ASSERT(fifo_dequeue(f, &k) == FIFO_EMPTY);
  CU_ASSERT(k == 436);

  CU_ASSERT(fifo_peak(f) == 32);

  fifo_destroy(f);
}

/*
  check that bad size argument results in a NULL fifo
*/

extern void test_fifo_bad_size(void)
{
  fifo_t *f = fifo_new(0, 5, 0);
  CU_ASSERT(f == NULL);
}

/*
  check that the max argument works as advertised
*/

extern void test_fifo_max(void)
{
  fifo_t *f = fifo_new(sizeof(int), 5, 10);
  CU_ASSERT(f != NULL);

  for (int i = 0 ; i < 10 ; i++)
    CU_ASSERT(fifo_enqueue(f, &i) == FIFO_OK);

  int k = 543;

  CU_ASSERT(fifo_enqueue(f, &k) == FIFO_USERMAX);

  fifo_destroy(f);
}

/*
  trigger a cheap contract
*/

extern void test_fifo_cheap_contract(void)
{
  fifo_t *f = fifo_new(sizeof(int), 4, 0);
  CU_ASSERT(f != NULL);

  for (int i = 0 ; i < 16 ; i++)
    CU_ASSERT(fifo_enqueue(f, &i) == FIFO_OK);

  for (int i = 0 ; i < 16 ; i++)
    {
      int j;

      CU_ASSERT(fifo_dequeue(f, &j) == FIFO_OK);
      CU_ASSERT(i == j);
    }

  fifo_destroy(f);
}

/*
  This forces an expand when the first element in the
  fifo is not at the start of the buffer
*/

extern void test_fifo_nontivial_expand(void)
{
  fifo_t *f;

  f = fifo_new(sizeof(int), 5, 0);
  CU_ASSERT(f != NULL);

  for (int i = 0 ; i < 4 ; i++)
    CU_ASSERT(fifo_enqueue(f, &i) == FIFO_OK);

  {
    int j;
    CU_ASSERT(fifo_dequeue(f, &j) == FIFO_OK);
    CU_ASSERT(0 == j);
  }

  for (int i = 4 ; i < 7 ; i++)
    CU_ASSERT(fifo_enqueue(f, &i) == FIFO_OK);

  for (int i = 1 ; i < 7 ; i++)
    {
      int j;
      CU_ASSERT(fifo_dequeue(f, &j) == FIFO_OK);
      CU_ASSERT(i == j);
    }

  fifo_destroy(f);
}
