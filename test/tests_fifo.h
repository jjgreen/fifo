/*
  tests_fifo.h
  Copyright (c) J.J. Green 2014
*/

#ifndef TESTS_FIFO_H
#define TESTS_FIFO_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_fifo[];
extern void test_fifo_fill_and_empty(void);
extern void test_fifo_bad_size(void);
extern void test_fifo_max(void);
extern void test_fifo_cheap_contract(void);
extern void test_fifo_nontivial_expand(void);

#endif
